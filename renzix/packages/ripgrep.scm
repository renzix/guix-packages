(define-module
  (renzix packages ripgrep)
  #:use-module (guix build-system cargo)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages crates-io))


(define-public rust-aho-corasick-0.7.6
  (package
    (name "rust-aho-corasick")
    (version "0.7.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "aho-corasick" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0b8dh20fhdc59dhhnfi89n2bi80a8zbagzd5c122hf1vv2amxysq"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/aho-corasick")
    (synopsis "A fast implementation of Aho-Corasick in Rust.")
    (description "A library for finding occurrences of many patterns at once
with SIMD acceleration in some cases. This library provides multiple pattern
search principally through an implementation of the Aho-Corasick algorithm,
which builds a finite state machine for executing searches in linear time.
Features include case insensitive matching, overlapping matches and search &
replace in streams.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-base64-0.10.0
  (package
    (name "rust-base64")
    (version "0.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "base64" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1qmrlwns4ckz75dxi1hap0a9bkljsrn3b5cvzgbqd3q0p3ncf7v2"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/marshallpierce/rust-base64")
    (synopsis "Base64 library in rust")
    (description "This library's goals are to be correct and fast. It's
thoroughly tested and widely used. It exposes functionality at multiple levels
of abstraction so you can choose the level of convenience vs performance that
you want, e.g. decode_config_slice decodes into an existing &mut [u8] and is
pretty fast (2.6GiB/s for a 3 KiB input), whereas decode_config allocates a new
Vec<u8> and returns it, which might be more convenient in some cases, but is
slower (although still fast enough for most purposes) at 2.1 GiB/s.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))


(define-public rust-bstr-0.2.0
  (package
    (name "rust-bstr")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "bstr" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1kc4qqwzq3zw2vf2jbzddz6wsr5v5fax1xn6ykbkvkimg1lvjc2b"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/bstr")
    (synopsis "A library for non utf8 strings")
    (description "This crate provides extension traits for &[u8] and Vec<u8>
that enable their use as byte strings, where byte strings are conventionally
UTF-8. This differs from the standard library's String and str types in that
they are not required to be valid UTF-8, but may be fully or partially valid
UTF-8.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:asl2.0))))

(define-public rust-bytecount-0.5.1
  (package
    (name "rust-bytecount")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "bytecount" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0z6a280kiy4kg5v3qw97pbyvwycr17fsm41804i8zpq7nmads3xy"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/llogiq/bytecount")
    (synopsis "Library to count occurrences of a given byte or UTF-8 characters in a slice of memory fast for rust")
    (description "This uses the \"hyperscreamingcount\" algorithm by Joshua Landau
to count bytes faster than anything else. The newlinebench repository has
further benchmarks for old versions of this repository.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-byteorder-1.3.2
  (package
    (name "rust-byteorder")
    (version "1.3.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "byteorder" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1xbwjlmq2ziqjmjvkqxdx1yh136xxhilxd40bky1w4d7hn4xvhx7"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/byteorder")
    (synopsis "Rust library for reading/writing numbers in big-endian and little-endian.")
    (description "Rust library for reading/writing numbers in big-endian and little-endian.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-crossbeam-channel-0.3.9
  (package
    (name "rust-crossbeam-channel")
    (version "0.3.9")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "crossbeam-channel" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1ylyzb1m9qbvd1nd3vy38x9073wdmcy295ncjs7wf7ap476pzv68"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/crossbeam-rs/crossbeam/tree/master/crossbeam-channel")
    (synopsis "Rust crate for message passing concurrency.")
    (description "This crate provides multi-producer multi-consumer channels for
message passing. It is an alternative to std::sync::mpsc with more features and
better performance.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-crossbeam-utils-0.6.6
  (package
    (name "rust-crossbeam-utils")
    (version "0.6.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "crossbeam-utils" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1rk0r9n04bmq4a3g2q5qhvvlmrmx780gc6h9lmc94mwndslkz5q4"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/crossbeam-rs/crossbeam/tree/master/crossbeam-utils")
    (synopsis "Rust library which provides msicellanious tools for concurrent programming")
    (description "Rust library which provides msicellanious tools for concurrent programming")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-encoding-rs-0.8.17
  (package
    (name "rust-encoding-rs")
    (version "0.8.17")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "encoding-rs" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1v902qqnbd37vdq4rjvp6k05wmghrasfdcjy30gp1xpjg5f7hma1"))))
    (build-system cargo-build-system)
    (home-page "https://docs.rs/encoding_rs/0.8.20/encoding_rs/")
    (synopsis "Encoding_rs is a Gecko-oriented Free Software / Open Source implementation of the Encoding Standard in Rust.")
    (description "encoding_rs is a Gecko-oriented Free Software / Open Source
implementation of the Encoding Standard in Rust. Gecko-oriented means that
converting to and from UTF-16 is supported in addition to converting to and from
UTF-8, that the performance and streamability goals are browser-oriented, and
that FFI-friendliness is a goal. Additionally, the mem module provides functions
that are useful for applications that need to be able to deal with legacy
in-memory representations of Unicode.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-encoding-rs-io-0.1.6
  (package
    (name "rust-encoding-rs-io")
    (version "0.1.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "encoding-rs-io" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0b7k9p7inkrcanh7h6q4m278y05gmcwi8p5r43h7grzl5dxfw6cn"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/encoding_rs_io")
    (synopsis "Streaming I/O adapters for the encoding_rs crate.")
    (description "This crate provides streaming adapters for the encoding_rs
crate. Adapters implement the standard library I/O traits and provide streaming
transcoding support.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-packed-simd-0.3
  (package
    (name "rust-packed-simd")
    (version "0.3.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "packed_simd" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0822wqf6kzw4ig9ykndg348w2bxkhs3x64brzsvdxh2a1pyajpm8"))))
    (build-system cargo-build-system)
    (home-page "https://rust-lang.github.io/packed_simd/packed_simd/")
    (synopsis "Portable Packed SIMD Vectors for Rust standard library.")
    (description "This crate exports Simd<[T; N]>: a packed vector of N elements
of type T as well as many type aliases for this type: for example, f32x4, which
is just an alias for Simd<[f32; 4]>.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-pcre2-0.2.1
  (package
    (name "rust-pcre2")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pcre2" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "103i66a998g1fjrqf9sdyvi8qi83hwglz3pjdcq9n2r207hsagb0"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/rust-pcre2")
    (synopsis "High level Rust bindings to PCRE2.")
    (description "A high level Rust wrapper library for PCRE2.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-pcre2-sys-0.2.1
  (package
    (name "rust-pcre2-sys")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pcre2-sys" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "10980c7q1634kcprmlx30rswb8rdlnwmg57vv87yw9dzpr1rssg9"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/rust-pcre2/tree/master/pcre2-sys")
    (synopsis "Bindings for PCRE2")
    (description "Bindings for PCRE2. Apart of pcre2.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))


(define-public rust-globset-0.4.4
  (package
    (name "rust-globset")
    (version "0.4.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "globset" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1wnqxq91liknmr2w93wjq2spyxbrd1pmnhd4nbi3921dr35a4nlj"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/globset")
    (synopsis "A globbing library for rust.")
    (description "Cross platform single glob and glob set matching. Glob set
matching is the process of matching one or more glob patterns against a single
candidate path simultaneously, and returning all of the globs that matched.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))


(define-public rust-grep-0.2.4
  (package
    (name "rust-grep")
    (version "0.2.4")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "grep" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1pkhjladybzzciwg0mjk3vjz5fyi76hk0d3hgyzv2jxlyp8v4fyc"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/grep")
    (synopsis "Ripgrep as a library")
    (description "Library that is intended to provide a high level facade to the
crates that make up ripgrep's core searching routines.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-grep-cli-0.1.3
  (package
    (name "rust-grep-cli")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "grep-cli" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "05a502x5m4fijwx7zj9icxna2dx86scm76ap80zr89pnvpbfk1hp"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/grep-cli")
    (synopsis "Utility library for the cli (part of ripgrep).")
    (description "A utility library that provides common routines desired in
search oriented command line applications. This includes, but is not limited to,
parsing hex escapes, detecting whether stdin is readable and more. To the extent
possible, this crate strives for compatibility across Windows, macOS and
Linux.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-grep-matcher-0.1.3
  (package
    (name "rust-grep-matcher")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "grep-matcher" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "113lafx3abrr96ahpz6yn905ian1w3qsr5hijbb909p2j0xgmhkm"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/grep-matcher")
    (synopsis "Low level interface to regex (part of ripgrep).")
    (description "This crate provides a low level interface for describing
regular expression matchers. The grep crate uses this interface in order to make
the regex engine it uses pluggable.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-grep-pcre2-0.1.3
  (package
    (name "rust-grep-pcre2")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "grep-pcre2" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1wjc3gsan20gapga8nji6jcrmwn9n85q5zf2yfq6g50c7abkc2ql"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/grep-pcre2")
    (synopsis "Implementation of grep-matcher to use PCRE2 (part of ripgrep).")
    (description "The grep-pcre2 crate provides an implementation of the Matcher
trait from the grep-matcher crate. This implementation permits PCRE2 to be used
in the grep crate for fast line oriented searching.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-grep-printer-0.1.3
  (package
    (name "rust-grep-printer")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "grep-printer" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0mxc1yx5sx89f00imlm5d3hxwdgglv9rzwdki8ba50gvq8a2nr8m"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/grep-printer")
    (synopsis "Print results from search (part of ripgrep).")
    (description "Print results from line oriented searching in a human readable, aggregate or JSON Lines format.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-grep-regex-0.1.5
  (package
    (name "rust-grep-regex")
    (version "0.1.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "grep-regex" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0afl67ikb42phn6fryxv2mmj97zb75llynr92fgzhin879c4vmm0"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/grep-regex")
    (synopsis "Implementation of grep-matcher (part of ripgrep).")
    (description "The grep-regex crate provides an implementation of the Matcher
trait from the grep-matcher crate. This implementation permits Rust's regex
engine to be used in the grep crate for fast line oriented searching.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-grep-searcher-0.1.6
  (package
    (name "rust-grep-searcher")
    (version "0.1.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "grep-searcher" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "09ag16im12v6k0lzkyvbvamn1iw15kfx1jbfldb7z5xa7208l04a"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/grep-searcher")
    (synopsis "High level library for exeuting fast line oriented searches (part of ripgrep).")
    (description "A high level library for executing fast line oriented
searches. This handles things like reporting contextual lines, counting lines,
inverting a search, detecting binary data, automatic UTF-16 transcoding and
deciding whether or not to use memory maps.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))


(define-public rust-ignore-0.4.7
  (package
    (name "rust-ignore")
    (version "0.4.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ignore" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "00mhksl41dnlsjqmka8c5a0m4spwm70ilm1qd9rngwq552hpzicd"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/ripgrep/tree/master/ignore")
    (synopsis "Rust library to swiftly filter items")
    (description "The ignore crate provides a fast recursive directory iterator
that respects various filters such as globs, file types and .gitignore files.
The precise matching rules and precedence is explained in the documentation for
WalkBuilder. Secondarily, this crate exposes gitignore and file type matchers
for use cases that demand more fine-grained control.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-lazy-static-1.1.0
  (package
    (name "rust-lazy-static")
    (version "1.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "lazy_static" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "19vincf2navflh6jlcysalbwyj78nc4mdfa5rlp0lyv5ln4qnj6a"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/rust-lang-nursery/lazy-static.rs")
    (synopsis "A small macro for defining lazy evaluated static variables in Rust.")
    (description "A macro for declaring lazily evaluated statics in Rust. Using
this macro, it is possible to have statics that require code to be executed at
runtime in order to be initialized. This includes anything requiring heap
allocations, like vectors or hash maps, as well as anything that requires
non-const function calls to be computed.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-memchr-2.2.1
  (package
    (name "rust-memchr")
    (version "2.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "memchr" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "13j6ji9x9ydpi9grbss106gqqr3xn3bcfp28aydqfa4751qrfmw8"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/rust-memchr")
    (synopsis "An implementation of memchr in Rust, with optional libc bindings.")
    (description "An implementation of memchr in Rust, with optional libc bindings.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-num-cpus-1.8.0
  (package
    (name "rust-num-cpus")
    (version "1.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "num_cpus" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0c0am9q3mxwdfyi59h4hah04v4y6mc18l5csml9d5adwwhi366n5"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/seanmonstar/num_cpus")
    (synopsis "Get the number of CPUs in Rust")
    (description "Library to count the number of cpus on the current machine")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-regex-1.2.1
  (package
    (name "rust-regex")
    (version "1.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "regex" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "09jww0faqvdprr9482ppxm1asbp6lhihr8zl9ma5sa4474cxkhw8"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/rust-lang/regex")
    (synopsis "An implementation of regular expressions for Rust.")
    (description "A Rust library for parsing, compiling, and executing regular
expressions. Its syntax is similar to Perl-style regular expressions, but lacks
a few features like look around and backreferences. In exchange, all searches
execute in linear time with respect to the size of the regular expression and
search text. Much of the syntax and implementation is inspired by RE2.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-regex-syntax-0.6.12
  (package
    (name "rust-regex-syntax")
    (version "0.6.12")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "regex-syntax" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "05pplicvzvgkb2wb4i98p2mrpgc8gws6vdl8xlpyyr6f3h6y59qi"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/rust-lang/regex/tree/master/regex-syntax")
    (synopsis "This crate provides a robust regular expression parser.")
    (description "There are two primary types exported by this crate: Ast and
Hir. The former is a faithful abstract syntax of a regular expression, and can
convert regular expressions back to their concrete syntax while mostly
preserving its original form. The latter type is a high level intermediate
representation of a regular expression that is amenable to analysis and
compilation into byte codes or automata. An Hir achieves this by drastically
simplifying the syntactic structure of the regular expression. While an Hir can
be converted back to its equivalent concrete syntax, the result is unlikely to
resemble the original concrete syntax that produced the Hir.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-ryu-1.0
  (package
    (name "rust-ryu")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ryu" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "15r9z2wzgbj04pks4jz7y6wif5xqhf1wqkl2nd7qrvn08ys68969"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/dtolnay/ryu")
    (synopsis "Fast floating point to string conversion in rust.")
    (description "Rustc Version 1.15+ Pure Rust implementation of Ryū, an
algorithm to quickly convert floating point numbers to decimal strings. The
PLDI'18 paper Ryū: fast float-to-string conversion by Ulf Adams includes a
complete correctness proof of the algorithm. The paper is available under the
creative commons CC-BY-SA license. This Rust implementation is a line-by-line
port of Ulf Adams' implementation in C")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   "boost"))))


(define-public rust-serde_json-1.0.23
  (package
    (name "rust-serde_json")
    (version "1.0.23")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "serde_json" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1xvr72815z1hn8wvjhgcw4fgs9dd30irqhm3xzsj3vikkgki1992"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/serde-rs/json")
    (synopsis "Strongly typed JSON library for Rust")
    (description "Serde is a framework for serializing and deserializing Rust
data structures efficiently and generically.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))

(define-public rust-jemallocator-0.3.2
  (package
    (name "rust-jemallocator")
    (version "0.3.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "jemallocator" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0sabfa5118b7l4ars5n36s2fjyfn59w4d6mjs6rrmsa5zky67bj3"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/gnzlbg/jemallocator")
    (synopsis "Rust allocator using jemalloc as a backend")
    (description "Rust allocator using jemalloc as a backend")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))


(define-public rust-regex-automata-0.1.8
  (package
    (name "rust-regex-automata")
    (version "0.1.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "regex-automata" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1y89vkwd9z7797lsdsizvhw4lw7i1mhfx97a8315bhkh2wm3rdwj"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/BurntSushi/regex-automata")
    (synopsis "A low level regular expression library that uses deterministic finite automata.")
    (description "A low level regular expression library that uses deterministic
finite automata. It supports a rich syntax with Unicode support, has extensive
options for configuring the best space vs time trade off for your use case and
provides support for cheap deserialization of automata for use in no_std
environments.")
    (properties '((hidden? . #t)))
    (license (list license:expat
                   license:public-domain))))

(define-public rust-strsim-0.8
  (package
    (name "rust-strsim")
    (version "0.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "strsim" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0sjsm7hrvjdifz661pjxq5w4hf190hx53fra8dfvamacvff139cf"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/dguo/strsim-rs")
    (synopsis "Rust library that implements string similarity metrics.")
    (description "This library implements string similarity metrics. Currently includes Hamming, Levenshtein, Jaro, and Jaro-Winkler.")
    (properties '((hidden? . #t)))
    (license license:expat)))

(define-public rust-textwrap-0.10.0
  (package
    (name "rust-textwrap")
    (version "0.10.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "textwrap" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "1xnkk81wksg87mc7mj71m44g7h14jnd6ya34vaa1zrwkkj38cxih"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/mgeisler/textwrap")
    (synopsis "Rust library for word wrapping text")
    (description "Textwrap is a small Rust crate for word wrapping text. You can
use it to format strings for display in commandline applications. The crate name
and interface is inspired by the Python textwrap module.")
    (properties '((hidden? . #t)))
    (license license:expat)))

(define-public rust-unicode-width-0.1.6
  (package
    (name "rust-unicode-width")
    (version "0.1.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "unicode-width" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "082f9hv1r3gcd1xl33whjhrm18p0w9i77zhhhkiccb5r47adn1vh"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/unicode-rs/unicode-width")
    (synopsis "Rust library to display width of Unicode characters and strings according to UAX#11 rules. ")
    (description "Determine displayed width of char and str types according to Unicode Standard Annex #11 rules.")
    (properties '((hidden? . #t)))
    (license (list license:asl2.0
                   license:expat))))


(define-public ripgrep
  (package
    (name "ripgrep")
    (version "11.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ripgrep" version))
       (file-name (string-append name "-" version ".crate"))
       (sha256
        (base32
         "0vqjr96s2rs45715hzf0g0wjahig4zjyiqfijmzzg4jyh9ni80yr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("aho-corasick" ,rust-aho-corasick-0.7.6)
        ("atty" ,rust-atty-0.2)
        ("base64" ,rust-base64-0.10.0)
        ("bitflags" ,rust-bitflags-1)
        ("bstr" ,rust-bstr-0.2.0)
        ("bytecount" ,rust-bytecount-0.5.1)
        ("byteorder" ,rust-byteorder-1.3.2)
        ("cc" ,rust-cc-1.0)
        ("cfg-if" ,rust-cfg-if-0.1)
        ("clap" ,rust-clap-2)
        ("crossbeam-channel" ,rust-crossbeam-channel-0.3.9)
        ("crossbeam-utils" ,rust-crossbeam-utils-0.6.6)
        ("encoding_rs" ,rust-encoding-rs-0.8.17)
        ("encoding_rs_io" ,rust-encoding-rs-io-0.1.6)
        ("fnv" ,rust-fnv-1.0)
        ("fs_extra" ,rust-fs-extra-1.1)
        ("glob" ,rust-glob-0.3)
        ("globset" ,rust-globset-0.4.4)
        ("grep" ,rust-grep-0.2.4)
        ("grep-cli" ,rust-grep-cli-0.1.3)
        ("grep-matcher" ,rust-grep-matcher-0.1.3)
        ("grep-pcre2" ,rust-grep-pcre2-0.1.3)
        ("grep-printer" ,rust-grep-printer-0.1.3)
        ("grep-regex" ,rust-grep-regex-0.1.5)
        ("grep-searcher" ,rust-grep-searcher-0.1.6)
        ("ignore" ,rust-ignore-0.4.7)
        ("itoa" ,rust-itoa-0.4)
        ("jemalloc-sys" ,rust-jemalloc-sys-0.3)
        ("jemallocator" ,rust-jemallocator-0.3.2)
        ("lazy_static" ,rust-lazy-static-1.3)
        ("libc" ,rust-libc-0.2)
        ("log" ,rust-log-0.4)
        ("num_cpus" ,rust-num-cpus-1.10)
        ("memchr" ,rust-memchr-2.2.1)
        ("memmap" ,rust-memmap-0.7)
        ("packed_simd" ,rust-packed-simd-0.3)
        ("pcre2" ,rust-pcre2-0.2.1)
        ("pcre2-sys" ,rust-pcre2-sys-0.2.1)
        ("pkg-config" ,rust-pkg-config-0.3)
        ("proc-macro2" ,rust-proc-macro2-1.0)
        ("quote" ,rust-quote-1.0)
        ("regex" ,rust-regex-1.2.1)
        ("regex-automata" ,rust-regex-automata-0.1.8)
        ("regex-syntax" ,rust-regex-syntax-0.6.12)
        ("ryu" ,rust-ryu-1.0)
        ("same-file" ,rust-same-file-1.0)
        ("serde_json" ,rust-serde-json-1.0)
        ("serde" ,rust-serde-1.0)
        ("serde_derive" ,rust-serde-derive-1.0)
        ("strsim" ,rust-strsim-0.8)
        ("syn" ,rust-syn-1.0)
        ("termcolor" ,rust-termcolor-1.0)
        ("textwrap" ,rust-textwrap-0.10.0)
        ("textwrap" ,rust-textwrap-0.11)
        ("thread_local" ,rust-thread-local-0.3)
        ("unicode-width" ,rust-unicode-width-0.1.6)
        ("unicode-xid" ,rust-unicode-xid-0.2)
        ("walkdir" ,rust-walkdir-2.2)
        ("winapi" ,rust-winapi-0.3)
        ("winapi-util" ,rust-winapi-util-0.1)
        ("wincolor" ,rust-wincolor-1.0))))
    (synopsis "ripgrep recursively searches directories for a regex pattern")
    (description "ripgrep is a line-oriented search tool that recursively
searches your current directory for a regex pattern. By default, ripgrep will
respect your .gitignore and automatically skip hidden files/directories and
binary files. ripgrep has first class support on Windows, macOS and Linux, with
binary downloads available for every release. ripgrep is similar to other
popular search tools like The Silver Searcher, ack and grep.")
    (home-page "https://github.com/BurntSushi/ripgrep")
    (license (list license:expat
                   license:public-domain))))
ripgrep
