(define-module
  (renzix packages derby)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages java)
  #:use-module (guix build-system ant)
  #:use-module ((guix licenses) #:prefix license:))

(define-public derby
  (package
    (name "derby")
    (version "10.15.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "http://mirror.metrocast.net/apache//db/derby/db-derby-" version "/db-derby-" version "-src.tar.gz"))
       (sha256
        (base32
         "00iccv0m35vf2f8vahzlb56w60rd97nkvfrk0xsvk7m6h5i0fzwy"))))
    (build-system ant-build-system)
    (arguments
     `(#:tests? #f
       #:jar-name "jars/sane/*.jar"
       #:jdk ,icedtea-8))
    (home-page "http://db.apache.org/derby/")
    (synopsis "Database written in java by apache")
    (description "Database written in java by apache")
    (license license:asl2.0)))

derby
