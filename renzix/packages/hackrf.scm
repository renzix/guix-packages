(define-module
  (renzix packages hackrf)
  #:use-module (guix build-system cmake)
  #:use-module (guix utils)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages libusb))

(define-public hackrf
  (package
    (name "hackrf")
    (version "2018.01.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Renzix/hackrf.git")
             (commit (string-append "v" version))))
       (file-name (string-append name "-" version "-checkout"))
       (sha256
        (base32
         "0idh983xh6gndk9kdgx5nzz76x3mxb42b02c5xvdqahadsfx3b9w"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f
       #:configure-flags (list "-DUDEV_RULES_GROUP=plugdev" "-DUDEV_RULES_PATH=lib/udev/rules.d")
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'enter-source
           (lambda _ (chdir "host") #t)))))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (inputs
     `(("libusb" ,libusb)
       ("fftwf" ,fftwf)))
    (home-page "http://greatscottgadgets.com/hackrf/")
    (synopsis "A open source SDR platform")
    (description "A open source SDR platform made to work with the hackrf")
    (license license:gpl2)))
hackrf
